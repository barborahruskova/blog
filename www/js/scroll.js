var $root = $('html, body');
$(document).ready(function() {
    $('a[href^="#"]').click(function(e) {
        var href = $.attr(this, 'href');

        let menu = $("#menu");

        $root.animate({
            scrollTop: $(href).offset().top - menu.height()
        }, 500);

        e.preventDefault();
    });
});